# Use an official Node.js runtime as the base image
FROM node:16.8.0

# Set the working directory in the container
WORKDIR /app

# Copy package.json and package-lock.json to the container
COPY package*.json ./

# Install dependencies
RUN npm i

# Copy the rest of the application code to the container
COPY . .

# Build the application
RUN npm run build

COPY . .
