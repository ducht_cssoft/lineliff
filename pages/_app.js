import "../styles/globals.css";
import { useEffect, useState } from "react";

function MyApp({ Component, pageProps }) {
  const [profile, setProfile] = useState({});
  const [isInit, setIsinit] = useState(false);

  useEffect(() => {
    import("@line/liff").then(async (liff) => {
      // start
      console.log("start liff.init()...");
      await liff
        .init({ liffId: process.env.LIFF_ID, withLoginOnExternalBrowser: true })
        .then(() => {
          console.log("liff.init() done");
        })
        .catch((error) => {
          console.log(`liff.init() failed: ${error}`);
          if (!process.env.liffId) {
            console.info(
              "LIFF Starter: Please make sure that you provided `LIFF_ID` as an environmental variable."
            );
          }
        });
      liff
        .getProfile()
        .then((response) => {
          setIsinit(true);
          console.log("getProfile result: ", {
            response,
            getLanguage: liff.getLanguage(),
            getVersion: liff.getVersion(),
            isInClient: liff.isInClient(),
            isLoggedIn: liff.isLoggedIn(),
            getOS: liff.getOS(),
          });
          setProfile({
            ...response,
            getLanguage: liff.getLanguage(),
            getVersion: liff.getVersion(),
            isInClient: liff.isInClient(),
            isLoggedIn: liff.isLoggedIn(),
            getOS: liff.getOS(),
          });
        })
        .catch(() => {
          liff.login();
        });
    });
  }, []);

  if (!isInit) return <Component {...pageProps} />;
  return (
    <div>
      <Component {...pageProps} />
      <div className="home">
        <h1 className="home__title">Get LINE profile info result:</h1>
      </div>
      <h3 style={{ textAlign: "center" }}>
        Display name: {profile.displayName}
      </h3>
      <h3 style={{ textAlign: "center" }}>UserId: {profile.userId}</h3>
      <h3 style={{ textAlign: "center" }}>Language: {profile.getLanguage}</h3>
      <h3 style={{ textAlign: "center" }}>Version: {profile.getVersion}</h3>
      <h3 style={{ textAlign: "center" }}>
        Is In Client: {`${profile.isInClient}`}
      </h3>
      <h3 style={{ textAlign: "center" }}>
        Is Logged In: {`${profile.isLoggedIn}`}
      </h3>
      <h3 style={{ textAlign: "center" }}>OS: {profile.getOS}</h3>
    </div>
  );
}

export default MyApp;
